import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})

export class PlayerComponent implements OnInit {
  public audioCtx: AudioContext;
  public analyser: AnalyserNode;
  public audio: HTMLAudioElement;
  public bufferLength: number;
  public dataArray: Uint8Array;
  public canvas: HTMLCanvasElement | any = null;
  public canvasCtx: CanvasRenderingContext2D | any = null;
  public x: number = 0;
  public barWidth: number = 0;
  public barHeight: number = 0;
  public duration: number = 0;
  public currentTime: number = 0;
  public currentSong: any = null;
  public songs = [
    {
      src: "../../assets/songs/Moderat - The Mark (Blackbird Remix).mp3",
      image: "https://picsum.photos/536/353",
      artist: "Moderat",
      title: "The Mark",
    },
    {
      src: "../../assets/songs/Myd - Muchas.mp3",
      image: "https://picsum.photos/536/354",
      artist: "Myd",
      title: "Muchas",
    },
    {
      src: "../../assets/songs/Voiron - Voironizer.mp3",
      image: "https://picsum.photos/536/355",
      artist: "Voiron",
      title: "Voironizer",
    },
    {
      src: "../../assets/songs/Yamaha.mp3",
      image: "https://picsum.photos/536/356",
      artist: "Yamaha",
      title: "Yamaha",
    },
    {
      src: "./../assets/songs/Elle.mp3",
      image: "https://picsum.photos/536/357",
      artist: "1",
      title: "Elle",
    },
    {
      src: "./../assets/songs/Outlaws.mp3",
      image: "https://picsum.photos/536/358",
      artist: "1",
      title: "Outlaws",
    },
    {
      src: "./../assets/songs/Bridges.mp3",
      image: "https://picsum.photos/536/359",
      artist: "2",
      title: "Bridges",
    },
    {
      src: "./../assets/songs/Far Away Coast.mp3",
      image: "https://picsum.photos/536/360",
      artist: "Far",
      title: "Away Coast",
    },
    {
      src: "./../assets/songs/Hotline Miami Soundtrack _ Crystals.mp3",
      image: "https://picsum.photos/536/361",
      artist: "HMS",
      title: "Crystals",
    },
    {
      src: "./../assets/songs/Hotline Miami Soundtrack _ Daisuke.mp3",
      image: "https://picsum.photos/536/362",
      artist: "HMS",
      title: "Daisuke",
    },
    {
      src: "./../assets/songs/I LEARNED IT.wav",
      image: "https://picsum.photos/536/363",
      artist: "Not me",
      title: "I learned it",
    },
    {
      src: "./../assets/songs/Koresma - New Frontier.wav",
      image: "https://picsum.photos/536/364",
      artist: "Koresma",
      title: "New Frontier",
    },
    {
      src: "./../assets/songs/Turquoise.mp3",
      image: "https://picsum.photos/536/365",
      artist: "???",
      title: "Turquoise",
    },

    
  ];

  constructor() {
    this.audio = new Audio();
    this.audioCtx = new AudioContext();
    this.analyser = this.audioCtx.createAnalyser();
    this.bufferLength = this.analyser.frequencyBinCount;
    this.dataArray = new Uint8Array(this.bufferLength);
  }

  ngOnInit(): void {
    this.canvas = <HTMLCanvasElement>document.getElementById("canvas");
    this.canvasCtx = <CanvasRenderingContext2D>this.canvas.getContext("2d");
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    
    let audioSource: MediaElementAudioSourceNode = this.audioCtx.createMediaElementSource(this.audio); 
    audioSource.connect(this.analyser);
    this.analyser.connect(this.audioCtx.destination);
    this.analyser.fftSize = 2048;
    const bufferLength: number = this.analyser.frequencyBinCount;

    this.barWidth = this.canvas.width / bufferLength * 1,5;
    this.animate();
    this.checkAudio();
    setInterval(() => this.checkAudio(), 1000);
  }

  private getRandomTrack() {
    return Math.floor(Math.random() * (this.songs.length - 0) + 0);
  }
  private checkAudio() {
    const remainingTime = this.audio.duration - this.audio.currentTime;
    if (remainingTime < 5 || Number.isNaN(remainingTime)) {
      this.changeSong();
    }
  }

  private changeSong() {
    this.audio.pause();
    this.currentSong = this.songs[this.getRandomTrack()]
    this.audio.src = this.currentSong.src;
    this.audio.crossOrigin = "anonymous";
    this.audio.play();
    this.audio.muted = false;
  }

  private animate = () => {
    this.x = 0;
    this.canvasCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.analyser.getByteFrequencyData(this.dataArray);
    this.drawVisualizer();
    requestAnimationFrame(this.animate);
  }

  private drawVisualizer = () => {
    let barHeight;
    for (let i = 0; i < this.bufferLength; i++) {
      barHeight = this.dataArray[i] * 4; 
      this.canvasCtx.fillStyle = `rgb(255, 255, 255)`;
      this.canvasCtx.fillRect(this.x, this.canvas.height - barHeight, this.barWidth, barHeight);
      this.x += this.barWidth * 2;
    }
  }; 
}
