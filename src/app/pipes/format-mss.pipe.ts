import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatMSS'
})
export class FormatMSSPipe implements PipeTransform {

  transform(s: number): string {
    let minutes: number = Math.floor(s / 60);
    let seconds: number = s % 60;
    if (seconds) {
      seconds = Math.round(seconds);
    }
    return String(minutes).padStart(2, "0") + ":" + String(seconds).padStart(2, "0");
  }

}
