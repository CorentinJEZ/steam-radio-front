import { Component } from '@angular/core';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent {
  public songs = [
    {
      src: "../../assets/songs/Moderat - The Mark (Blackbird Remix).mp3",
      image: "https://picsum.photos/100/100",
      artist: "Moderat",
      title: "The Mark",
    },
    {
      src: "../../assets/songs/Myd - Muchas.mp3",
      image: "https://picsum.photos/536/354",
      artist: "Myd",
      title: "Muchas",
    },
    {
      src: "../../assets/songs/Voiron - Voironizer.mp3",
      image: "https://picsum.photos/536/35",
      artist: "Voiron",
      title: "Voironizer",
    },
    {
      src: "../../assets/songs/Yamaha.mp3",
      image: "https://picsum.photos/536/356",
      artist: "Yamaha",
      title: "Yamaha",
    },
    {
      src: "./../assets/songs/Elle.mp3",
      image: "https://picsum.photos/536/357",
      artist: "1",
      title: "Elle",
    },
    {
      src: "./../assets/songs/Outlaws.mp3",
      image: "https://picsum.photos/536/358",
      artist: "1",
      title: "Outlaws",
    },
    {
      src: "./../assets/songs/Bridges.mp3",
      image: "https://picsum.photos/536/359",
      artist: "2",
      title: "Bridges",
    },
    {
      src: "./../assets/songs/Far Away Coast.mp3",
      image: "https://picsum.photos/536/360",
      artist: "Far",
      title: "Away Coast",
    },
    {
      src: "./../assets/songs/Hotline Miami Soundtrack _ Crystals.mp3",
      image: "https://picsum.photos/536/361",
      artist: "HMS",
      title: "Crystals",
    },
    {
      src: "./../assets/songs/Hotline Miami Soundtrack _ Daisuke.mp3",
      image: "https://picsum.photos/536/362",
      artist: "HMS",
      title: "Daisuke",
    },
    {
      src: "./../assets/songs/I LEARNED IT.wav",
      image: "https://picsum.photos/536/363",
      artist: "Not me",
      title: "I learned it",
    },
    {
      src: "./../assets/songs/Koresma - New Frontier.wav",
      image: "https://picsum.photos/536/364",
      artist: "Koresma",
      title: "New Frontier",
    },
    {
      src: "./../assets/songs/Turquoise.mp3",
      image: "https://picsum.photos/536/365",
      artist: "???",
      title: "Turquoise",
    },

    
  ];

  public selectTrack(){
    alert("oui")
  }
}
